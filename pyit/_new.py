"""
New module
"""

import sys

import pyit
from pyit.settings import settings


def main():
    """
    Module entrypoint
    """
    settings.logger.info("New package {}".format(pyit.__version__))
    sys.exit(0)


if __name__ == "__main__":
    main()
