"""
New Project package
"""

import warnings

from pyit.interfaces import *

warnings.simplefilter("ignore")

__version__ = "2021.1.11b4"
